<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Temperature</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <?php
    $sat = 34;
    $sun = 32;
    $mon = 31;
    $tue = 32;
    $wed = 33;
    $thu = 34;
    $fri = 34;
    $avg = ($sat + $sun + $mon + $tue + $wed + $thu + $fri) / 7;

    $max = max($sat , $sun , $mon , $tue , $wed , $thu , $fri);
    $min = min($sat , $sun , $mon , $tue , $wed , $thu , $fri)


    ?>

<div class="container col-md-6" style="margin-top: 1in">
<table class="table table-bordered">
  <thead>
    <tr>
      <th class="col-md-2">Day</th>
      <th class="col-md-2">Temperature</th>
      <th class="col-md-2">Average Temperature</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Sat</td>
      <?php 
        if( $max == $sat){
        echo "<td style='color:red;'> 
        $sat
        </td>";
        }
        elseif( $min == $sat)
          echo "<td style='color:blue;'> 
          $sat
          </td>";
        else{
          echo "<td>$sat</td>";
          }

      ?>
      
    <td class="align-middle" rowspan="7"><?php echo round($avg, 2); ?></td>
    </tr>
    <tr>
      <td>Sun</td>
      <?php 
        if( $max == $sun){
        echo "<td style='color:red;'> 
        $sun
        </td>";
        }
        elseif( $min == $sun)
          echo "<td style='color:blue;'> 
          $sun
          </td>";
          else{
            echo "<td>$sun</td>";
          }
      ?>
    </tr>
    <tr>
      <td>Mon</td>
      <?php 
        if( $max == $mon){
        echo "<td style='color:red;'> 
        $mon
        </td>";
        }
        elseif( $min == $mon)
          echo "<td style='color:blue;'> 
          $mon
          </td>";
          else{
            echo "<td>$mon</td>";
          }
      ?>
    </tr>
    <tr>
      <td>Tue</td>
      <?php 
        if( $max == $tue){
        echo "<td style='color:red;'> 
        $tue
        </td>";
        }
        elseif( $min == $tue)
          echo "<td style='color:blue;'> 
          $tue
          </td>";
          else{
            echo "<td>$tue</td>";
          }
      ?>
    </tr>    
    <tr>
      <td>Wed</td>
      <?php 
        if( $max == $wed){
        echo "<td style='color:red;'> 
        $wed
        </td>";
        }
        elseif( $min == $wed)
          echo "<td style='color:blue;'> 
          $wed
          </td>";
          else{
            echo "<td>$wed</td>";
          }
      ?>
    </tr>
    <tr>
      <td>Thu</td>
      <?php 
        if( $max == $thu){
        echo "<td style='color:red;'> 
        $thu
        </td>";
        }
        elseif( $min == $thu)
          echo "<td style='color:blue;'> 
          $thu
          </td>";
          else{
            echo "<td>$thu</td>";
          }
      ?>
    </tr>
    <tr>
      <td>Fri</td>
      <?php 
        if( $max == $fri){
        echo "<td style='color:red;'> 
        $fri
        </td>";
        }
        elseif( $min == $fri)
          echo "<td style='color:blue;'> 
          $fri
          </td>";
          else{
            echo "<td>$fri</td>";
          }
      ?>
    </tr>
  </tbody>

</table>
<p class="text-center">* Maximum temperature color : <span style="color:red">Red</span> &nbsp&nbsp & &nbsp&nbsp Minimum temperature color : <span style="color:blue">Blue</span></p>
</div>
</body>
</html>
