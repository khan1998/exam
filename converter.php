<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Converter</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>

    <div class="container col-md-6" style="margin-top: 1in">
	
		<h1 class="converter-title">Converter</h1>

		<input type="text" placeholder="Input" id="input">
		<span>=</span>
		<input type="text" placeholder="Result" id="result">

        <select  id="inputType">
			<option value="pound">Pound</option>
			<option value="kg">Kilogran</option>
		</select>
        <select  id="resultType">
			<option value="kg">Kilogram</option>
			<option value="pound">Pound</option>
		</select>

	</div>

<script>
var input = document.getElementById('input');
var result = document.getElementById('result');
var inputType = document.getElementById('inputType');
var resultType = document.getElementById('resultType');
var option_from,option_to;


input.addEventListener("keyup",myResult);
inputType.addEventListener("change",myResult);
resultType.addEventListener("change",myResult);


option_from = inputType.value;
option_to   = resultType.value;


function myResult(){

	option_from = inputType.value;
	option_to = resultType.value;



	if(option_from === "kg" && option_to==="pound"){

		result.value = Number(input.value) *2.2046;
	}


	if(option_from === "pound" && option_to==="kg"){

		result.value = Number(input.value) * 0.453592;
	}

    if(option_from === "pound" && option_to==="pound"){

        result.value = input.value
    }
    if(option_from === "kg" && option_to==="kg"){

        result.value = input.value
    }
	

}

</script>

</body>
</html>
