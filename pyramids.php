

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pyramids</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container-md" style="margin-top: 1in; margin-left:3in">
        <div class="row">
            <div class="col-md-4" style="background-color:#2a329317;">
                <?php
                    $num = 6;

                    for($i=1; $i<=$num; $i++)
                    {
                        for($j=0; $j<$i; $j++)
                        {
                            echo "* &nbsp&nbsp";
                        }
                        echo "<br>";
                    }
                    for($i=$num-1; $i>=1; $i--)
                    {
                        for($j=0; $j<$i; $j++)
                        {
                            echo "* &nbsp&nbsp";
                        }
                        echo "<br>";
                    }
                ?>
            </div>
            <div class="col-md-4" style="background-color:#95ff9536;">
                <?php
                    $num = 7;
                    for($i=0; $i<$num; $i++)
                    {
                        for($j=0; $j<=$i; $j++)
                        {
                            echo $j*$i ." &nbsp&nbsp&nbsp";
                        }
                        echo "<br>";
                    }

                ?>
            </div>

        </div>
    </div>
</body>
</html>
